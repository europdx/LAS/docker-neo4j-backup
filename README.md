# Docker-neo4j-backup


Usage:

## Backup

<code>docker run -v path_1:/backup -v volume_1:/neodata:ro backup</code>

where

<code>path_1</code> is host directory where backups should be stored
<code>volume_1</code> is volume which should be backuped


## Recover
<code>docker run -it -v ~/neo4j.tgz:/neo4j.tgz -v las-docker_neodata:/neodata test recover</code>