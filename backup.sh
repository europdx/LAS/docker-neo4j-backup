#!/bin/sh


if [ "$1" = "recover" ]; then
  rm -rf /neodata/*
  echo "Starting Neo4j database recovery."
  ls /one-backup
  tar -xvzf /one-backup/neo4j.tgz -C /
  exit 0
fi

echo "Starting backup in container."
BACKUP_DIR=/backup/$ENV_NOW/$ENV_NOWT/neo4j

mkdir -p $BACKUP_DIR

echo "Backup dir is $BACKUP_DIR."
echo "Creating tgz file."
tar -zcvf $BACKUP_DIR/neo4j.tgz /neodata/*
